The Benchmarks Game
===================

The project has only recently moved to GitLab, so things are a little messed-up — please show some patience!

Website
-------

Toy-program performance measurements for ~24 language implementations.

https://benchmarksgame-team.pages.debian.net/benchmarksgame/

Questions?
----------

Please open a new [Question](https://salsa.debian.org/benchmarksgame-team/benchmarksgame/issues/new?issuable_template=Question) issue.

Suggestions?
------------

Please open a new [Change](https://salsa.debian.org/benchmarksgame-team/benchmarksgame/issues/new?issuable_template=Change) issue.


Contributing
------------

We are very happy to accept community contributions. Please refer to [CONTRIBUTING.md](/CONTRIBUTING.md) for details.

Available data
--------------

We are happy for you to use [the program measurements](/public/data/) to do your own analysis and presentation.


Archive
-------

We are happy for you to use the contributed source-code files in your own projects.

Previously contributed (Alioth benchmarks game project and Alioth "shootout" project) source-code files are available from the [archive-alioth-benchmarksgame](https://salsa.debian.org/benchmarksgame-team/archive-alioth-benchmarksgame) project.
